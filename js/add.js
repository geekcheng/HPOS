// JavaScript Document
var $ = function (id) {  
    return "string" == typeof id ? document.getElementById(id) : id;  
}

function goprevpage()
{
    parent._ifContent.src = "./page/"+pageInstance[0].Prevpage+".html";
}
function getnumber(val)
{
	return (!isNaN(val));
}

var AddTemplate = Class.create();  
AddTemplate.prototype = {  
    //初始化  
    Init:function(options){  
	    this.Document = document;
		this.Window = window;
	    this.SetOptions(options);//设置参数 
		this.sqlProvider = this.Options.sqlProvider;
	    this.sqltablename = this.Options.sqltablename;
		this.sqltableitem = this.Options.sqltableitem;
	
		this.tableitem = this.Options.tableitem;
		this.nullmsg = this.Options.nullmsg;
		this.remark = this.Options.remark;
          
        this.Prepage=this.Options.prevpage;  
        this.Nextpage=this.Options.nextpage;  
       
		this._pageIndex = 0;//当前页
    },  
	
	//设置参数  
    SetOptions:function(options){  
        this.Options={  
            prevpage:"0",        //前一页  
            nextpage:"0",       //后一页 
			sqltablename:"",
			sqltableitem:[],
			tableitem:[],
			nullmsg:[]
		
        };  
        Extend(this.Options,options||{});  
    }, 
	goprevpage:function(){
		 parent._ifContent.src = "./page/goods.html";
	},
	
	//添加数据
	addtodatabase:function()
	{
		
		for(var i=0;i<this.sqltableitem.length;i++)
		{
			var pageitem = $(this.tableitem[i]);
			if(pageitem.value==""&&this.nullmsg[i]!="")
			{
			pageitem.placeholder = this.nullmsg[i];
			//pageitem.focus();
			return;
			}
			if(pageitem.value!=""&&this.remark[i]=="number")
			{
				if(!getnumber(pageitem.value))
				{
					pageitem.value="";
			     pageitem.placeholder = "只能输入数字";
			    // pageitem.focus();
			     return;
				}
			}
			
		}
		
	//商品条码，商品名称，商品单价存到数据库中  
	
	var loadstring = "select * from "+this.sqltablename;
	loadstring += " WHERE ";
	loadstring += this.sqltableitem[0];
	loadstring +="=?";
	

	var insertvalue = new Array();
	for(var i=0;i<this.sqltableitem.length;i++)
	{
		
		insertvalue.push($(this.tableitem[i]).value); 
		
	}
	sqltableitem = this.sqltableitem;
	
	var sqltablename = this.sqltablename;
	this.sqlProvider.insertRow(sqltablename,sqltableitem,insertvalue,function(id)
				{
					 parent._admintips="添加成功";
					 window.parent.gotopage("tealist");
				//return to prev 
				//	this.table.InsertNewRow([id,code_id,goods_name,goods_unit,goods_sellperprice,goods_discount]); 
					
				//	hiddendiv('LayerUpdate'); 
					//sAlert(null,{width:380,height:120,left:322,top:240},"添加成功!",1); 
			//		showAllData();
				});
	
	},
	//修改数据
	updatetodatabase:function()
	{
		for(var i=0;i<this.sqltableitem.length;i++)
		{
			var pageitem = $(this.tableitem[i]);
			if(pageitem.value==""&&this.nullmsg[i]!="")
			{
			pageitem.placeholder = this.nullmsg[i];
			//pageitem.focus();
			return;
			}
			if(pageitem.value!=""&&this.remark[i]=="number")
			{
				if(!getnumber(pageitem.value))
				{
					pageitem.value="";
			     pageitem.placeholder = "只能输入数字";
			    // pageitem.focus();
			     return;
				}
			}
			
		}
		var insertvalue = new Array();
	
	for(var i=0;i<this.sqltableitem.length;i++)
	{
		
		insertvalue.push($(this.tableitem[i]).value); 
		
	}
		sqltableitem = this.sqltableitem;
	
	var sqltablename = this.sqltablename;
	//商品条码，商品名称，商品单价存到数据库中  
	this.sqlProvider.updateRow(sqltablename,sqltableitem,insertvalue,parent._selectedrowid,function(id)
				{
					 parent._admintips="修改成功";
					 window.parent.gotopage("tealist");
				//return to prev 
				//	this.table.InsertNewRow([id,code_id,goods_name,goods_unit,goods_sellperprice,goods_discount]); 
					
				//	hiddendiv('LayerUpdate'); 
					//sAlert(null,{width:380,height:120,left:322,top:240},"添加成功!",1); 
			//		showAllData();
				});
	
	},
	//删除数据库里的数据
	deletetodatabase:function()
	{
		
		var sqltablename = this.sqltablename;
	    this.sqlProvider.deleteRow(sqltablename,parent._selectedrowid,function(result){
			 //showdiv
			 parent._admintips="删除成功";
			 window.parent.gotopage("tealist");
			});
	},
	//初始化修改数据
	updateinit:function()
	{
		
		
	//商品条码，商品名称，商品单价存到数据库中  
	
	var loadstring = "select * from "+this.sqltablename;
	loadstring += " WHERE id=?";
	
	
	sqltableitem = this.sqltableitem;
	tableitem = this.tableitem;
	
	var sqltablename = this.sqltablename;
	this.sqlProvider.loadTable(loadstring,[parent._selectedrowid],function(result){	
	
		if(result.rows.length==1)
		{
			var row = result.rows.item(0);
			for(var i=0;i<sqltableitem.length;i++)
		    {
			  var pageitem = tableitem[i];
			  var param = sqltableitem[i];
			  $(pageitem).value = eval("row."+param);
			  
			// window.parent.gotopage("teatype");
			
		    }
			
		}
		else
		{
			alert("此商品不存在");
		//	showmsg("LayerUpdate","code_id","商品条码不可重复");						
		//	$("code_id").focus();
			//return;
		}
	}); 
	},
};  
function adddata()
{
	addInstance.addtodatabase();
}
function updatetodatabase()
{
	addInstance.updatetodatabase();
}
function deletedata()
{
	addInstance.deletetodatabase();
}
function cancelclick()
{
	parent._admintips="";
	 window.parent.gotopage("tealist");
}

function init()
{
	
	this.sqlProvider = window.parent.sqlProvider;
	sqlProvider = this.sqlProvider;
	var divinner="";
//	divinner += "<div class='layer1'>";
//	divinner += "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;商品编码&nbsp;</span>";
	//divinner += "<input type='text' id='goods_codeid' placeholder='请输入商品编码'>";
	divinner += "</div>";
	divinner += "<div class='layer2'>";
	divinner += "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;商品名称&nbsp;</span>";
	divinner += "<input type='text' id='goods_name' placeholder='请输入商品名称'>";
	divinner += "</div>";
	divinner += "<div class='layer3'>";
	divinner += "<span>零售单价(元)&nbsp;</span>";
	divinner += "<input type='text' id='goods_sellperprice' placeholder='请输入商品零售价'>";
	divinner += "</div>";
	
	
	var bdiv ="";
	bdiv += "<div id='addsure' class='baddsure'>确定</div>";
	bdiv +=  "<div id='cancel' class='bcancel'>取消</div>";
	$("LayerB").innerHTML = bdiv;
	this.addInstance=new AddTemplate({ sqltablename:"TEA_INFO",sqltableitem:["goods_name","goods_sellperprice"],tableitem:["goods_name","goods_sellperprice"],nullmsg:["商品名称不可为空","商品单价不可为空"],remark:["","number"],sqlProvider:this.sqlProvider});
	addInstance = this.addInstance;
	
	if(parent._operatetype=="add") 
	{
		$("LayerAdd").innerHTML = divinner;
	//	$("goods_name").focus();
		addEventHandler($("addsure"),"click",adddata); 
	}
	else if(parent._operatetype=="update")
	{
		$("LayerAdd").innerHTML = divinner;
		
		addInstance.updateinit();
		//$("goods_name").focus();
		addEventHandler($("addsure"),"click",updatetodatabase); 
	}
	else if(parent._operatetype=="delete")
	{
		divinner = "<div >";
		divinner += "<p>确定删除吗？</p>";
		divinner += "</div>";
		
		$("LayerAdd").innerHTML = divinner;
		addEventHandler($("addsure"),"click",deletedata); 
		
	}
	addEventHandler($("cancel"),"click",cancelclick); 
}
var addInstance;
window.onload = init;