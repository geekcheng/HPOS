
var ClassPrinter ={
	   create: function() {
                return function() { this.Init.apply(this, arguments); }  
         }  
}

var Printer = ClassPrinter.create(); 

Printer.prototype = { 
	Init:function()
	{
		this.device=null;
		
		if( window.TAP != null )
    {
    	this.device = TAP.Device.PrinterService.ReceiptPrinter;
      return;
    }
    if( window.parent.TAP != null )
    {
        this.device =  parent.TAP.Device.PrinterService.ReceiptPrinter;
        return;
    }
    if( window.parent.parent.TAP != null )
    {
        this.device =  parent.parent.TAP.Device.PrinterService.ReceiptPrinter;
        return;
    }
    if( window.parent.parent.parent.TAP != null )
    {
        this.device =  parent.parent.parent.TAP.Device.PrinterService.ReceiptPrinter;
        return;
    }   
	}
		
 
}





